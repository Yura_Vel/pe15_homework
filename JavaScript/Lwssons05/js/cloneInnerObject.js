function makeClone(obj) {
    let clone = {}; // Создаем новый пустой объект
    for (let prop in obj) { // Перебираем все свойства копируемого объекта
        if (obj.hasOwnProperty(prop)) { // Только собственные свойства
            if ("object"===typeof obj[prop]) // Если свойство так же объект
                clone[prop] = makeClone(obj[prop]); // Делаем клон свойства
            else
                clone[prop] = obj[prop]; // Или же просто копируем значение
        }
    }
    return clone;
}