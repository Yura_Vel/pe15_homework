const cardInput = document.querySelector('.card');
cardInput.addEventListener('input', onInput);

//8761-xx5xx-xxxx-xxx
//8761//subtask
//87615xxxxxxxxx...//subtas2
//8765-3xxx-xxxx-xxxx
function onInput(evt) {
    const value = evt.target.value;
    const alldigits = getDigits(value);      //3
    const digits = leaveDigits(alldigits);
    const digitsWithX = fillNumberWithX(digits);  //3xxxxxxxxx
    const separatedDigitWithX = addDelimeters(digitsWithX);
    evt.target.value = separatedDigitWithX;

}

//8761-xx5xx-xxxx-xxx--->87615

function getDigits(cardNumberOnChange) {
    let digits = '';
    for (let i = 0; i < cardNumberOnChange.length; i++) {
        const char = cardNumberOnChange[i];
        if (!isNaN(+char)) {
            digits += char;
        }
    }
    return digits;


}


function leaveDigits(cardNumber) {
    return cardNumber.slice(0, 16);

}

//87615xxxxxxxxx...//subtas2

function fillNumberWithX(cardNumber) {
    cardNumber.length;
    let x = '';
    for (let i = 0; i < 16 - cardNumber.length; i++) {
        x += "X";
    }
    return cardNumber + x;


    //return (new Array(16-cardNumber.length)).fill('x').join('');

    return cardNumber + 'X'.repeat(16 - cardNumber.length);

}

//87615xxxxxxxxx--->8761-5xxx-xxxx-xxxx

function addDelimeters(cardNumber) {

    return `${cardNumber.slice(0, 4)}-${cardNumber.slice(4, 8)}-${cardNumber.slice(8, 12)}-${cardNumber.slice(12)}`


}



