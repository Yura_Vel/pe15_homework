const input=document.querySelector('input');
const button=document.querySelector('button');


input.addEventListener('focus',onFocus);
input.addEventListener('blur',onBlur);

button.addEventListener('focus',onFocus);
button.addEventListener('blur',onBlur);

function onFocus(event){
    //event.target---element under action
    //event.currentTarget
    event.target.style.transform='scale(2)';

}

function onBlur(event) {
    event.target.style.transform='';
}