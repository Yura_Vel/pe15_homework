const parent=document.querySelector('.parent');
const child1=document.querySelector('.child:nth-child(1)');
const child2=document.querySelector('.child:nth-child(2)');

addListeners(parent,'parent');
addListeners(child1,'child1');
addListeners(child2,'child2');


function addListeners(element,name){
    element.addEventListener('mouseout',()=>console.log(`mouseout${name}`));
    element.addEventListener('mouseover',()=>console.log(`mouseover${name}`));
    element.addEventListener('mouseleave',()=>console.log(`mouseleave${name}`));
    element.addEventListener('mouseenter',()=>console.log(`mouseenter${name}`));
}




parent.addEventListener('click',onParentClick);


function onParentClick(event) {
    console.log(event);

}